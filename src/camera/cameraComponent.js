import React, {Component} from 'react';

import {
  View,
  StyleSheet,
  Image,
  Text,
  Alert,
  TouchableOpacity,
  Permission,
  PermissionsAndroid,
  Platform,
} from 'react-native';

import {RNCamera} from 'react-native-camera';
import RNFetchBlob from 'rn-fetch-blob';
import PushNotification from 'react-native-push-notification';


export class CameraComponent extends Component {
  constructor(props) {
    super(props);
    

    this.state = {
      takingPic: false,
      storeagePermission: false,
      camera: true,
      angle: RNCamera.Constants.Type.back,
    };
  }

  componentDidMount() {
    this.checkPermissionAndGrant();
  }

  //function to check permission for storage
  checkPermissionAndGrant() {
    PermissionsAndroid.check(
      PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
    ).then(isPermiited => {
      if (isPermiited) {
        this.setState(
          {
            storeagePermission: true,
          },
          () => console.log(this.state.storeagePermission),
        );
      } else {
        PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        ).then(data => {
          this.setState(
            {
              storeagePermission: true,
            },
            () => console.log('here', this.state.storeagePermission),
          );
        });
      }
    });
  }

  //function to change the camera
  changeCamera() {
    if (this.state.camera) {
      this.setState({
        camera: false,
      });
    }
    if (!this.state.camera) {
      this.setState({
        camera: true,
      });
    }
    if (this.state.camera === true) {
      this.setState({
        angle: RNCamera.Constants.Type.back,
      });
    }

    if (this.state.camera === false) {
      this.setState({
        angle: RNCamera.Constants.Type.front,
      });
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          style={styles.preview}
          type={this.state.angle}
          playSoundOnCapture={true}
          flashMode={RNCamera.Constants.FlashMode.on}>
          <TouchableOpacity
            onPress={this.changeCamera.bind(this)}
            style={styles.change}>
            <Image
              style={styles.changeImg}
              source={require('./changeImg.png')}
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={this.takePicture.bind(this)}
            style={styles.capture}>
            <Image
              style={styles.captureImage}
              source={require('./capture.png')}
            />
          </TouchableOpacity>
        </RNCamera>

        <View
          style={{
            flex: 0,
            flexDirection: 'row',
            justifyContent: 'center',
          }}></View>
      </View>
    );
  }

  takePicture = async () => {
    if (this.camera && !this.state.takingPic) {
      let options = {
        quality: 0.85,
        base64: true,
      };

      this.setState({
        takingPic: true,
      });

      try {
        const data = await this.camera.takePictureAsync(options);
        console.log(data);
        const splited = data.uri.split('/');
        const imageName = splited[splited.length - 1];
        console.log('image-name----------->', imageName);

        const folderPath = '/storage/emulated/0/camStreak';
        const filePath = folderPath + '/' + imageName;

        RNFetchBlob.fs.isDir(folderPath).then(isDir => {
          if (isDir) {
            console.log('dir hai');
            this.savePicture(filePath, data);
          } else {
            RNFetchBlob.fs.mkdir(folderPath).then(() => {
              console.log('dir created');
              console.log(data);
              this.savePicture(filePath, data);
            });
          }
        });
      } catch (err) {
        Alert.alert('Error', 'Failed to take picture: ' + (err.message || err));
        return;
      } finally {
        this.setState({takingPic: false});
      }
    }
  };

  //function to save pictures
  savePicture(filePath, data) {
    RNFetchBlob.fs
      .createFile(filePath, JSON.stringify(data.base64), 'base64')
      .then(() => {
        RNFetchBlob.fs.scanFile([{path: filePath}]).then(() => {
          PushNotification.createChannel(
            {
              channelId: "007", // (required)
              channelName: "notification for channel", // (required)
              channelDescription: "A channel to categorise your notifications", // (optional) default: undefined.
              playSound: true, // (optional) default: true
              soundName: "default", // (optional) See `soundName` parameter of `localNotification` functio
              vibrate: true, // (optional) default: true. Creates the default vibration patten if true.
            },(created) => console.log(`createChannel returned '${created}'`)),

          PushNotification.localNotification({
            channelId: "007",
            
            message: 'your photo saved to the gallery ', // (required)
          });
          console.log('scaned and saved');
          return;
        });
      });
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'black',
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  capture: {
    flex: 0,
    // backgroundColor: '#fff',
    // borderRadius: 100,

    paddingHorizontal: 20,
    // alignSelf: 'center',
    margin: 10,
  },
  captureImage: {
    width: 70,
    height: 70,
  },
  change: {
    flex: 1,
    marginTop: 10,
    marginLeft: 330,
    // marginBottom:600
  },
  changeImg: {
    width: 40,
    height: 40,
  },
});

export default CameraComponent;
