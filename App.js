import React from 'react';

import CameraComponent from './src/camera/cameraComponent';

export default class App extends React.Component {
    render() {
        return (
            <CameraComponent />
        );
    };
};